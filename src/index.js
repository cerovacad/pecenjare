import React from "react";
import ReactDOM from "react-dom";
import MapWithAMarker from "./map";
import { API_KEY } from "./utils/consts";

const App = () => (
  <div>
    <MapWithAMarker
      googleMapURL={API_KEY}
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `900px` }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
  </div>
);

export default App;

ReactDOM.render(<App />, document.getElementById("root"));

