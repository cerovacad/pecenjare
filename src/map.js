import React, { Component } from "react";
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";
import axios from "axios";
import CustomMarker from "./marker";

class Map extends Component {
  state = {
    grills: []
  };

  componentWillMount() {
    axios
      .get(`http://localhost:3000/grills`)
      .then(res => {
        navigator.geolocation.getCurrentPosition(position => {
          this.setState({
            grills: res.data,
            position: {
              lng: position.coords.longitude,
              lat: position.coords.latitude
            }
          });
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    return (
      <div>
        <GoogleMap
          Control
          options={{
            styles: [
              {
                featureType: "all",
                elementType: "labels.text",
                stylers: [
                  {
                    visibility: "off"
                  }
                ]
              },
              {
                featureType: "poi",
                elementType: "labels.icon",
                stylers: [
                  {
                    visibility: "off"
                  }
                ]
              }
            ],
            streetViewControl: true,
            mapTypeControl: false
          }}
          defaultZoom={12}
          center={this.state.position}
        >
          {this.state.grills.map(grill => {
            return <CustomMarker key={grill.id} grill={grill} />;
          })}
        </GoogleMap>
      </div>
    );
  }
}

export default withScriptjs(withGoogleMap(Map));
