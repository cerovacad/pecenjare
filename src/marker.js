import React from "react";
import { Marker } from "react-google-maps";

const icon = {
  url: "pig.svg",
  scaledSize: { width: 35, height: 35 }
};

const CustomMarker = ({ grill }) => (
  <div>
    <Marker
      icon={icon}
      position={{ lat: grill.latitude, lng: grill.longitude }}
    />
  </div>
);

export default CustomMarker;
